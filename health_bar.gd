class_name HealthBar extends ProgressBar

@onready var damage_bar: ProgressBar = $DamageBar

var health: int = 0

func _ready():
	var a = get_node("DamageBar")
	print("DamageBar node1 ", a.value)

func set_health(_value: int):
	health = _value
	value = _value
	print("HealthBar children", get_children())
	var a = get_node("DamageBar")
	print("DamageBar node ", a.value)
	damage_bar.value = _value
