extends CharacterBody2D

const PROGRESS_BAR: PackedScene = preload("res://progress_bar.tscn")
@onready var healthBar = PROGRESS_BAR.instantiate()

func _ready() -> void:
	healthBar.set_health(100)
	self.add_child(healthBar)
